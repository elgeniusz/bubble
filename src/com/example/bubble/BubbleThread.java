package com.example.bubble;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.Handler;
import android.view.SurfaceHolder;

public class BubbleThread extends Thread {
	private int canvasWidth = 200;
	private int canvasHeight = 200;
	private boolean run = false;
	
	private float bubbleX;
	private float bubbleY;
	private float dirX = 0;
	private float dirY = 0;
	private static final float MAX_SPEED = 10;
	private static final float SPEED_FACTOR = 10;
	private static final int WIDTH = 40;
	private static final int HEIGHT = 10;
	
	private SurfaceHolder sh;
	private Context ctx;
	private Handler hdlr;
	private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
	
	public BubbleThread(SurfaceHolder surfaceHolder, Context context, Handler handler) {
		sh = surfaceHolder;
		hdlr = handler;
		ctx = context;
		paint.setColor(Color.BLACK);
		paint.setStyle(Style.FILL);
	}
	
	protected void setBubble(float x, float y)
	{
		bubbleX = x;
		bubbleY = y;
	}
	
	protected void moveBubbleX(float x)
	{	
		dirX = x / SPEED_FACTOR;
		
		if(Math.abs(dirX) > MAX_SPEED)
		{
			if(dirX > 0)
			{
				dirX = MAX_SPEED;
			}
			else
			{
				dirX = -MAX_SPEED;
			}
		}
	}
	
	protected void moveBubbleY(float y)
	{
		dirY = y / SPEED_FACTOR;
		
		if(Math.abs(dirY) > MAX_SPEED)
		{
			if(dirY > 0)
			{
				dirY = MAX_SPEED;
			}
			else
			{
				dirY = -MAX_SPEED;
			}
		}
	}
	
	public void doStart() {
		synchronized(sh) {
			//Starts bubble in center and do some random motion
			this.bubbleX = this.canvasWidth / 2;
			this.bubbleY = this.canvasHeight;
		}
	}
	
	public void run() {
		while(run) {
			Canvas c = null;
			try {
				c = sh.lockCanvas(null);
				synchronized(sh) {
					doDraw(c);
				}
			} finally {
				if(c != null) {
					sh.unlockCanvasAndPost(c);
				}
			}
		}
	}
	
	public void setRunning(boolean b) {
		run = b;
	}
	
	public void setSurfaceSize(int width, int height) {
		synchronized (sh) {
			canvasWidth = width;
			canvasHeight = height;
			doStart();
		}
	}
	
	public void doDraw(Canvas canvas) {
		if(run) {
			bubbleX = bubbleX + dirX;
			bubbleY = bubbleY + dirY;
			
			if(bubbleX - WIDTH < 0)
			{
				bubbleX = WIDTH;
			}
			
			if(bubbleX + WIDTH > canvasWidth)
			{
				bubbleX = canvasWidth - WIDTH;
			}
			
			if(bubbleY - HEIGHT < 0)
			{
				bubbleY = HEIGHT;
			}
			
			if(bubbleY + HEIGHT > canvasHeight)
			{
				bubbleY = canvasHeight - HEIGHT;
			}
			
			canvas.restore();
			canvas.save();
			canvas.rotate(dirX, bubbleX, bubbleY);
			canvas.drawColor(Color.WHITE);
			Rect rect = new Rect((int)Math.round(bubbleX) - WIDTH, (int)Math.round(bubbleY) - HEIGHT, 
										(int)Math.round(bubbleX) + WIDTH, (int)Math.round(bubbleY) + HEIGHT);
			canvas.drawRect(rect, paint);
			canvas.restore();
		}
	}
}
