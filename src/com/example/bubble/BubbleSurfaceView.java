package com.example.bubble;

import android.content.Context;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

public class BubbleSurfaceView extends SurfaceView 
										implements SurfaceHolder.Callback {
	private BubbleThread thread;
	private SurfaceHolder sh;
	private Context ctx;
	
	private static final int INVALID_POINTER_ID = -1;
	private int activePointerLeft = INVALID_POINTER_ID;
	private int activePointerRight = INVALID_POINTER_ID;
	private float yLeft;
	private float xRight;
	
	private int surfaceWidth;
	
	public boolean onTouchEvent(MotionEvent e)
	{
		int action = e.getActionMasked();
		switch(action) 
		{
		case MotionEvent.ACTION_DOWN:
		//case MotionEvent.ACTION_POINTER_DOWN:
			int pointerIndex = e.getActionIndex();
			//Left DPad
			if(e.getX() < surfaceWidth/2)
			{
				activePointerLeft = e.getPointerId(pointerIndex);
				//Storing initial coordinates of pointer
				yLeft = e.getY();
			}
			//Right DPad
			else
			{
				activePointerRight = e.getPointerId(pointerIndex);
				//Storing initial coordinates of pointer
				xRight = e.getX();
			}
			break;
		case MotionEvent.ACTION_MOVE:
			if(activePointerLeft != INVALID_POINTER_ID)
			{
				int pointerIndexRL = e.findPointerIndex(activePointerLeft);
				thread.moveBubbleY(e.getY(pointerIndexRL) - yLeft);
			}
			
			if(activePointerRight != INVALID_POINTER_ID)
			{
				int pointerIndexRL = e.findPointerIndex(activePointerRight);
				thread.moveBubbleX(e.getX(pointerIndexRL) - xRight);
			}
			break;
		case MotionEvent.ACTION_CANCEL:
			activePointerLeft = INVALID_POINTER_ID;
			activePointerRight = INVALID_POINTER_ID;
			thread.moveBubbleX(0);
			thread.moveBubbleY(0);
			break;
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_POINTER_UP:
			int activePointerIndex = e.getActionIndex();
			int pointerId = e.getPointerId(activePointerIndex);
			
			if(pointerId == activePointerRight)
			{
				activePointerRight = INVALID_POINTER_ID;
				thread.moveBubbleX(0);
			}
			else if(pointerId == activePointerLeft)
			{
				activePointerLeft = INVALID_POINTER_ID;
				thread.moveBubbleY(0);
			}
			break;
		default:
			return true;
		}
		return true;
	}
	
	public BubbleSurfaceView(Context context){
		super(context);
	    sh = getHolder();
	    sh.addCallback(this);
		ctx = context;
		setFocusable(true);
	}
	
	public BubbleThread getThread() {
		return thread;
	}

	public void surfaceCreated(SurfaceHolder holder) {
		thread = new BubbleThread(sh, ctx, new Handler());
		thread.setRunning(true);
		thread.start();
	}
	
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		surfaceWidth = width;
		thread.setSurfaceSize(width, height);
	}
	
	public void showTotalTime(long time)
	{
		Toast.makeText(ctx, "Touch time in ms was: " + time, Toast.LENGTH_SHORT).show();
	}
	
	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;
		thread.setRunning(false);
		while(retry) {
			try {
				thread.join();
				retry = false;
			} catch (InterruptedException e) {
			}
		}
	}
}
